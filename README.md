# nobl9-action

This action is applying a nobl9 configuration file (specified at `SLOCTL_YML` input parameter) to a project using `sloctl` (https://nobl9.github.io/techdocs_Sloctl_User_Guide) tool.

# Requirements

- A valid nobl9 account must be activated (check https://nobl9.com for more information)

# Usage

This action supports GitLab pipeline secrets in all the input parameters. More details: https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project.

Example:
```yaml
variables:
  CLIENT_ID: $NOBL9_CLIENT_ID
  CLIENT_SECRET: $NOBL9_CLIENT_SECRET
  ACCESS_TOKEN: $NOBL9_ACCESS_TOKEN
  PROJECT: $NOBL9_PROJECT
  SLOCTL_YML: $SLOCTL_YML

include:
  - project: 'craciunctln/nobl9-action'
    ref: main
    file: '/.gitlab-ci.yml'
```
